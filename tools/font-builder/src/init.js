const { execSync } = require('child_process')
const { accessSync, constants, mkdirSync } = require('fs')
const { resolve } = require('path')

const ora = require('ora')

module.exports = function init(quiet = true) {
  const dir = process.cwd()
  const spinner = ora(`Initializing font-builder at ${dir}`).start()

  const venv = resolve(dir, '.venv')
  try {
    accessSync(venv, constants.F_OK)
  } catch (err) {
    mkdirSync(venv, { recursive: true })
  }

  const fontbakery = resolve(dir, '.venv/fontbakery')
  try {
    accessSync(fontbakery, constants.F_OK)
  } catch (err) {
    execSync(`python3 -m venv ${fontbakery}`, { stdio: quiet ? 'ignore' : 'inherit' })
  }

  const fontmake = resolve(dir, '.venv/fontmake')
  try {
    accessSync(fontmake, constants.F_OK)
  } catch (err) {
    execSync(`python3 -m venv ${fontmake}`, { stdio: quiet ? 'ignore' : 'inherit' })
  }

  const gftools = resolve(dir, '.venv/gftools')
  try {
    accessSync(gftools, constants.F_OK)
  } catch (err) {
    execSync(`pip2 install --upgrade virtualenv`, { stdio: quiet ? 'ignore' : 'inherit' })
    execSync(`virtualenv -p python2 ${gftools}`, { stdio: quiet ? 'ignore' : 'inherit' })
  }

  const woff2 = resolve(dir, '.woff2')
  try {
    accessSync(woff2, constants.F_OK)
  } catch (err) {
    execSync(`git clone --recursive https://github.com/google/woff2.git ${woff2}`, {
      stdio: quiet ? 'ignore' : 'inherit',
    })
  }

  spinner.succeed()
}

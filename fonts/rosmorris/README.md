# Rosmorris Typeface

> A utility sans-serif typeface for UI

Rosmorris is a utility typeface intended to be the standard UI font across all of Talamh's websites and apps. It is a
variable font that has been carefully designed to be highly readable, even at small sizes.

Rosmorris is currently a work in progress and does not support many characters at the moment.

Rosmorris is the capital city of _Fréamhú_ the initially conceived region of Talamh. Since the typeface is meant to be
a utility font it is being made in the spirit of New York City's use of Helvetica for its municipal signage.

## Install

```
npm install @talamh/rosmorris
```

## Usage

Within your CSS:

```css
@import '@talamh/rosmorris';
```

## Contributing

PRs accepted.

## License

Font licensed under [SIL Open Font License 1.1][ofl] © Jeremy Boles.

[ofl]: ./LICENSE

import { readdirSync } from 'fs'
import { resolve } from 'path'

import * as sapper from '@sapper/server'
import compression from 'compression'
import polka from 'polka'
import sirv from 'sirv'

const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === 'development'

const fonts = readdirSync(resolve('../../fonts'), { withFileTypes: true })
  .filter(f => f.isDirectory())
  .map(f => sirv(resolve('../../fonts', f.name, 'dist'), { dev }))

polka() // You can also use Express
  .use(compression({ threshold: 0 }), sirv('static', { dev }), ...fonts, sapper.middleware())
  .listen(PORT, err => {
    if (err) console.error('error', err)
  })

# Talamh Standard Sizing

> Talamh's sizing units

Talamh uses standard sizing units to add visual harmony and consistency to
page elements. The size values are provided as CSS variables that can be
included in any project through a bundler or using something like
[a PostCSS plugin][postcss-import].

Talamh's sizes are roughly based off the [golden ratio][ratio], rounding both
down and up to whole numbers when nessessary.

## Install

```
npm install @talamh/sizing
```

## Usage

Within your CSS:

```css
@import '@talamh/sizing';
```

## Contributing

PRs accepted.

## License

Source code licensed under [AGPLv3][agpl] © Jeremy Boles.

[agpl]: ./LICENSE
[postcss-import]: https://github.com/postcss/postcss-import
[ratio]: https://en.wikipedia.org/wiki/Golden_ratio

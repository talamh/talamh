const { basename } = require('path')

module.exports = {
  plugins: [
    require('postcss-import'),
    require('postcss-rem'),
    require('postcss-easings'),

    require('postcss-preset-env')({
      browsers: 'last 2 versions',
      features: {
        'color-mod-function': { importFrom: [require.resolve('@talamh/colors/styles/index.css')] },
      },
      stage: 3,
    }),

    require('postcss-url')({
      filter: /\.woff2?$/,
      url: asset => `/${basename(asset.absolutePath)}`,
    }),
  ],
}

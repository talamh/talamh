import { toggleMark } from 'prosemirror-commands'
import { schema } from 'prosemirror-schema-basic'
import { Plugin } from 'prosemirror-state'

import { el } from './ContextualToolbar.svelte'

class SelectionToolbar {
  constructor(view) {
    view.dom.parentNode.appendChild(el)

    el.addEventListener('mousedown', event => {
      event.preventDefault()
      view.focus()

      if (el.contains(event.target)) {
        const command = toggleMark(schema.marks[event.target.dataset.mark])
        command(view.state, view.dispatch, view)
      }
    })

    this.buttons = el.querySelectorAll(':scope > button')

    this.update(view, null)
  }

  update(view, lastState) {
    const { state } = view

    // Don't do anything if the document/selection didn't change
    if (lastState && lastState.doc.eq(state.doc) && lastState.selection.eq(state.selection)) return

    // Hide the toolbar if the selection is empty
    if (state.selection.empty) {
      el.setAttribute('aria-hidden', 'true')
      return
    }

    const { from, to } = state.selection

    // Otherwise, reposition it and update its content
    el.setAttribute('aria-hidden', 'false')

    // These are in screen coordinates
    const start = view.coordsAtPos(from)
    const end = view.coordsAtPos(to)

    // The box in which the tooltip is positioned, to use as base
    const box = el.offsetParent.getBoundingClientRect()
    const dim = el.getBoundingClientRect()

    // Find a center-ish x position from the selection endpoints (when
    // crossing lines, end may be more to the left)
    const left = Math.max((start.left + end.right) / 2, start.left)

    el.style.left = (left - dim.width / 2 - 5).toString() + 'px'
    el.style.bottom = box.bottom - start.top + 5 + 'px'

    // Deal with the button states
    this.buttons.forEach(button => {
      const active = state.doc.rangeHasMark(from, to, schema.marks[button.dataset.mark])
      button.setAttribute('aria-checked', active)
    })
  }

  destroy() {
    el.remove()
  }
}

export default function selectionToolbar() {
  return new Plugin({
    view(editorView) {
      return new SelectionToolbar(editorView)
    },
  })
}

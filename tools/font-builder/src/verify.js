const { execSync } = require('child_process')
const { readdirSync } = require('fs')
const { extname, resolve } = require('path')

const ora = require('ora')

module.exports = function verify(src, dist, quiet = true) {
  const path = resolve(process.cwd(), '.venv/fontbakery/bin')

  const fontbakery = `${path}/fontbakery`
  const pip = `${path}/pip`

  execSync(`${pip} install --upgrade pip`, { stdio: quiet ? 'ignore' : 'inherit' })
  execSync(`${pip} install fontbakery`, { stdio: quiet ? 'ignore' : 'inherit' })
  execSync(`${pip} install ufolint`, { stdio: quiet ? 'ignore' : 'inherit' })

  const spinner = ora(`Verifying fonts`).start()

  //
  // First, check that the UFO files look good
  // -------------------------------------------------------------------------------------------------

  const srcs = readdirSync(src, { withFileTypes: true })
    .filter(f => f.isDirectory())
    .map(f => `${src}/${f.name}`)

  if (srcs.length > 0) {
    const files = srcs.join(' ')
    const cmd = `PATH=${path}:$PATH ${fontbakery} check-ufo-sources ${files}`
    execSync(cmd, { stdio: quiet ? 'ignore' : 'inherit' })
  }

  //
  // Now, check that the the outputted OTF files look okay
  // -------------------------------------------------------------------------------------------------

  const fonts = readdirSync(dist, { withFileTypes: true })
    .filter(f => f.isFile() && extname(f.name) === '.ttf')
    .map(f => `${dist}/${f.name}`)

  if (fonts.length > 0) {
    const files = fonts.join(' ')
    const cmd = `PATH=${path}:$PATH ${fontbakery} check-opentype ${files}`
    execSync(cmd, { stdio: quiet ? 'ignore' : 'inherit' })
  }

  //
  // Also, check that the the outputted files generally look okay
  // -------------------------------------------------------------------------------------------------

  if (fonts.length > 0) {
    const files = fonts.join(' ')
    const cmd = `PATH=${path}:$PATH ${fontbakery} check-universal ${files}`
    execSync(cmd, { stdio: quiet ? 'ignore' : 'inherit' })
  }

  spinner.succeed()
}

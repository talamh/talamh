export default {
  pub: {
    menu: {
      changeEditorFont: 'Change Editor Font…',
      deleteSection: 'Delete Section',
      enterFullscreen: 'Enter Fullscreen',
      exportContent: 'Export Content',
      focusMode: 'Focus Mode',
      monospace: 'Monospace',
      sansSerif: 'Sans-Serif',
      serif: 'Serif',
      showSubsections: 'Show Subsections',
    },
  },
}

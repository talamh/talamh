# Talamh Color Scheme

> Talamh's base color scheme

Talamh uses a standard color scheme to bring cohesion to the world and the
_brand_, such as it is. The color values are provided as CSS variables that
can be included in any project through a bundler or using something like
[a PostCSS plugin][postcss-import].

Talamh's color scheme has been derived from the [Gruvbox][gruvbox] terminal
theme but has been slightly modified for use in a beyond syntax highlighting.

## Install

```
npm install @talamh/colors
```

## Usage

Within your CSS:

```css
@import '@talamh/colors';
```

## Contributing

PRs accepted.

## License

Colors licensed under [CC-BY-SA-4.0][cc] © Jeremy Boles.
Source code licensed under [AGPLv3][agpl] © Jeremy Boles.

[agpl]: ./LICENSE.AGPL
[cc]: ./LICENSE.CC-BY-SA
[gruvbox]: https://github.com/morhetz/gruvbox
[postcss-import]: https://github.com/postcss/postcss-import

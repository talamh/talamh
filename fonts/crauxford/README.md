# Crauxford Typeface

> A monospace typeface for writing

Crauxford is an in-progress monospace typeface that is optimized for writing prose.
The font is available as a single variable TTF file and contains regular and italic
versions. The bold and bold italic versions are forthcoming.

## Install

```
npm install @talamh/crauxford
```

## Usage

Within your CSS:

```css
@import '@talamh/crauxford';
```

## Contributing

PRs accepted.

## License

Font licensed under [SIL Open Font License 1.1][ofl] © Jeremy Boles.

[ofl]: ./LICENSE

# Teorann Typeface

> A serif typeface for reading and writing prose

Teorann is an in-progress serif typeface that is designed to be easy on the
eyes when reading and writing long spans of prose. The font is available as
a single variable TTF file and contains regular, italic, bold, and bold
italic versions.

## Install

```
npm install @talamh/teorann
```

## Usage

Within your CSS:

```css
@import '@talamh/teorann';
```

## Contributing

PRs accepted.

## License

Font licensed under [SIL Open Font License 1.1][ofl] © Jeremy Boles.

[ofl]: ./LICENSE

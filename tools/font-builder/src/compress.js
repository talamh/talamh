const { execSync } = require('child_process')
const { accessSync, constants, readdirSync } = require('fs')
const { extname, resolve } = require('path')

const ora = require('ora')

module.exports = function compress(dist, quiet = true) {
  const woff2 = resolve(process.cwd(), '.woff2')
  const compress_woff2 = `${woff2}/woff2_compress`

  const spinner = ora(`Compressing OTFs at ${dist}`).start()

  try {
    accessSync(compress_woff2, constants.X_OK)
  } catch (err) {
    execSync('git pull', { cwd: woff2, stdio: quiet ? 'ignore' : 'inherit' })
    execSync('make clean all', { cwd: woff2, stdio: quiet ? 'ignore' : 'inherit' })
  }

  const makeWOFF =
    execSync(`which sfnt2woff`)
      .toString()
      .trim().length !== 0

  const fonts = [].concat(
    ...readdirSync(dist, { withFileTypes: true })
      .filter(f => f.isFile() && extname(f.name) === '.ttf')
      .map(f => `${dist}/${f.name}`)
  )

  fonts.forEach(f => {
    if (makeWOFF) execSync(`sfnt2woff ${f}`, { stdio: quiet ? 'ignore' : 'inherit' })
    execSync(`${compress_woff2} ${f}`, { stdio: quiet ? 'ignore' : 'inherit' })
  })

  spinner.succeed()
}

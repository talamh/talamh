const { execSync } = require('child_process')
const { mkdirSync, readdirSync } = require('fs')
const { basename, extname, resolve } = require('path')

const ora = require('ora')

module.exports = function build(src, dist, individual = true, quiet = true) {
  const fontmake = resolve(process.cwd(), '.venv/fontmake/bin/fontmake')
  const pip = resolve(process.cwd(), '.venv/fontmake/bin/pip3')

  execSync(`${pip} install --upgrade pip`, { stdio: quiet ? 'ignore' : 'inherit' })
  execSync(`${pip} install fontmake`, { stdio: quiet ? 'ignore' : 'inherit' })

  //
  // Build all of the UFO projects into OTF files
  // -------------------------------------------------------------------------------------------------

  if (individual) {
    const ufoSpinner = ora(`Building UFO sources at ${src}`).start()

    const ufoSources = readdirSync(src, { withFileTypes: true })
      .filter(f => f.isDirectory())
      .map(f => `${src}/${f.name}`)

    if (ufoSources.length > 0) {
      const files = ufoSources.join(' ')
      const cmd = `${fontmake} --autohint --output ttf --output-dir ${dist} --ufo-paths ${files} --timing --validate-ufo`
      execSync(cmd, { stdio: quiet ? 'ignore' : 'inherit' })
    }

    ufoSpinner.succeed()
  }

  //
  // Build all of the design spaces into variable TTF files
  // -------------------------------------------------------------------------------------------------

  try {
    mkdirSync(dist)
  } catch (e) {}

  const designspaceSpinner = ora(`Building variable fonts at ${src}`).start()

  const designspaceSources = readdirSync(src, { withFileTypes: true })
    .filter(f => f.isFile() && extname(f.name) === '.designspace')
    .map(f => `${src}/${f.name}`)

  designspaceSources.forEach(s => {
    const typefaceName = basename(s, '.designspace')
    const cmd = `${fontmake} --autohint --mm-designspace ${s} --output variable --output-path ${dist}/${typefaceName}.ttf --round-instances --timing --validate-ufo`
    execSync(cmd, { stdio: quiet ? 'ignore' : 'inherit' })
  })

  designspaceSpinner.succeed()
}

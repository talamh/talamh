module.exports = {
  env: { browser: true, es6: true, node: true },
  extends: ['eslint:recommended', 'plugin:prettier/recommended'],
  parserOptions: {
    allowImportExportEverywhere: true,
    ecmaVersion: 11,
    sourceType: 'module',
  },

  overrides: [{ files: ['**/*.svelte'], processor: 'svelte3/svelte3' }],
  plugins: ['import', 'eslint-plugin-import-order-alphabetical', 'svelte3'],

  rules: {
    'import/no-named-as-default-member': 'off',
    'import/named': 'off',
    'import/order': ['error', { 'newlines-between': 'always-and-inside-groups' }],
    'import-order-alphabetical/order': 'error',
    'no-console': ['error', { allow: ['debug', 'error', 'info', 'warn'] }],
    'no-empty-pattern': 'off',
  },

  settings: {
    'import/resolver': 'node',
  },
}

const path = require('path')

module.exports = {
  plugins: [path.resolve('./common/temp/node_modules/prettier-plugin-svelte')],
  printWidth: 120,
  semi: false,
  singleQuote: true,
  trailingComma: 'es5',
}

const { execSync } = require('child_process')
const { readFileSync, readdirSync, unlinkSync, writeFileSync } = require('fs')
const { extname, resolve } = require('path')

const ora = require('ora')

module.exports = function postprocess(dist, quiet = true) {
  const gftools = resolve(process.cwd(), '.venv/gftools/bin/gftools')
  const pip = resolve(process.cwd(), '.venv/gftools/bin/pip2')
  const ttx = resolve(process.cwd(), '.venv/gftools/bin/ttx')

  const spinner = ora(`Processing the built fonts`).start()

  execSync(`${pip} install git+https://github.com/googlefonts/gftools`, { stdio: quiet ? 'ignore' : 'inherit' })

  const fonts = readdirSync(dist, { withFileTypes: true })
    .filter(f => f.isFile() && extname(f.name) === '.ttf')
    .map(f => `${dist}/${f.name}`)

  if (fonts.length > 0) {
    const files = fonts.join(' ')
    const cmd = `${gftools} fix-dsig --autofix ${files}`
    execSync(cmd, { stdio: quiet ? 'ignore' : 'inherit' })
  }

  fonts.forEach(font => {
    execSync(`${ttx} ${font}`, { stdio: quiet ? 'ignore' : 'inherit' })

    const ttxFile = font.replace('.ttf', '.ttx')
    const xml = readFileSync(ttxFile).toString()

    if (xml.includes('<MVAR>')) {
      const updated = xml.replace(/<MVAR>[^]*<\/MVAR>/, '')
      writeFileSync(ttxFile, updated)

      unlinkSync(font)
      execSync(`${ttx} ${ttxFile}`, { stdio: quiet ? 'ignore' : 'inherit' })
    }
    unlinkSync(ttxFile)
  })

  spinner.succeed()
}

import resolve from 'rollup-plugin-node-resolve'
import svelte from 'rollup-plugin-svelte'

import pkg from './package.json'

const name = pkg.name
  .replace(/^(@\S+\/)?(svelte-)?(\S+)/, '$3')
  .replace(/^\w/, m => m.toUpperCase())
  .replace(/-\w/g, m => m[1].toUpperCase())

export default {
  input: 'src/index.svelte',
  output: [{ file: pkg.module, format: 'es' }, { file: pkg.main, format: 'umd', name }],
  plugins: [svelte(), resolve()],
  onwarn(warning) {
    // Suppress this error message...
    // https://github.com/rollup/rollup/wiki/Troubleshooting#this-is-undefined
    if (warning.code === 'THIS_IS_UNDEFINED') return
    console.error(warning.message)
  },
}

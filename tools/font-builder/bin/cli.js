#!/usr/bin/env node

const { resolve } = require('path')

const commandLineArgs = require('command-line-args')

const build = require('../src/build')
const compress = require('../src/compress')
const init = require('../src/init')
const postprocess = require('../src/postprocess')
const verify = require('../src/verify')

const options = commandLineArgs([
  { name: 'destination', alias: 'd', multiple: true, type: String, defaultValue: './dist' },
  { name: 'individual', alias: 'i', type: Boolean },
  { name: 'no-build', alias: 'n', type: Boolean },
  { name: 'src', type: String, multiple: true, defaultOption: true },
  { name: 'test', alias: 't', type: Boolean },
  { name: 'verbose', type: Boolean },
])

init(!options.verbose)

options.src.forEach((d, i) => {
  if (!options['no-build']) {
    build(
      resolve(process.cwd(), d),
      resolve(process.cwd(), options.destination[i]),
      !!options.individual,
      !options.verbose
    )
    compress(resolve(process.cwd(), options.destination[i]), !options.verbose)
    postprocess(resolve(process.cwd(), options.destination[i]), !options.verbose)
  }

  if (!!options.test)
    verify(resolve(process.cwd(), d), resolve(process.cwd(), options.destination[i]), !options.verbose)
})

const sveltePreprocess = require('svelte-preprocess')

module.exports = exports = {
  preprocess: sveltePreprocess({ postcss: true }),
}

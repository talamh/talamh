# Westward Quay Typeface

> A san-serif typeface for reading and writing prose

Westward Quay is an in-progress san-serif typeface that is designed to be
easy on the eyes when reading and writing long spans of prose. The font is
available as a single variable TTF file and contains regular, italic, bold,
and bold italic versions.

## Install

```
npm install @talamh/westward-quay
```

## Usage

Within your CSS:

```css
@import '@talamh/westward-quay';
```

## Contributing

PRs accepted.

## License

Font licensed under [SIL Open Font License 1.1][ofl] © Jeremy Boles.

[ofl]: ./LICENSE
